import React, { PureComponent } from 'react';
import { PanelProps, PanelPlugin } from '@grafana/ui';

export class MyPanel extends PureComponent<PanelProps> {
  state = {
    time: new Date(),
  };
  start() {
    setInterval(() => this.setState({ time: new Date() }), 1000);
  }
  componentWillMount() {
    this.start();
  }
  render() {
    return (
      <div>
        <div style={{ textAlign: 'center', fontSize: '20px', color: 'white' }}>{this.state.time.toLocaleString()}</div>
      </div>
    );
  }
}

export const plugin = new PanelPlugin(MyPanel);
