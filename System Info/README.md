# System Information table
Plugin will display system information data.
Configure Elasticsearch datasource and push data of system metrics using metricbeat.
Then use raw document query

```
npm install
npm run build
```
Then put dist directory inside $GRAFANA_HOME/data/plugins Folder.

This will run linting tools and apply prettier fix.


To build the plugin run:
```
yarn build
```
