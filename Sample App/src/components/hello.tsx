import React, { PureComponent } from 'react';

export class Hello extends PureComponent {
  render() {
    return <div>Hello from page</div>;
  }
}
