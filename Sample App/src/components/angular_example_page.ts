export class AngularExamplePageCtrl {
  static templateUrl = 'components/angular_example_page.html';

  /** @ngInject */
  constructor($scope: any, $rootScope: any) {
    console.log('AngularExamplePageCtrl:', this);
  }
}
