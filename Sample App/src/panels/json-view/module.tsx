import React, { PureComponent } from 'react';
import { PanelProps, PanelPlugin } from '@grafana/ui';
import './app.css';
export class MyPanel extends PureComponent<PanelProps> {
  convert<HTMLAttributes>(data: any): any {
    console.log(data);
    //const value: object[] = JSON.parse(data);
    const val = Object.assign(data.buffer[0].host, data.buffer[0].host.os);
    delete val.os;
    return Object.entries(val).map(i => {
      if (typeof i[0] === 'string' && typeof i[1] === 'string') {
        return (
          <tr>
            <td>{i[0]}</td>
            <td>{i[1]}</td>
          </tr>
        );
      }
      return <></>;
    });
  }
  render() {
    const time = this.props.data.series[0].fields[0].values;

    return (
      <div>
        <table className="table table-bordered table-striped table-hover">
          <thead>
            <th>Type</th>
            <th>Value</th>
          </thead>
          {this.convert(time)}
        </table>
      </div>
    );
  }
}

export const plugin = new PanelPlugin(MyPanel);
