///<reference path="../node_modules/grafana-sdk-mocks/app/headers/common.d.ts" />
import { AppConfigCtrl } from './config/config';
import { loadPluginCss } from 'grafana/app/plugins/sdk';
import { AngularExamplePageCtrl } from './components/angular_example_page';
loadPluginCss({
  dark: 'plugins/crest-sampleapp/styles/dark.css',
  light: 'plugins/crest-sampleapp/styles/light.css',
});
export { AppConfigCtrl as ConfigCtrl, AngularExamplePageCtrl };
