# Simple React App

This is just a stub to show how you can create a basic visualization plugin.

To work with this plugin run:

```
npm install
npm run build
```
Then put dist directory inside $GRAFANA_HOME/data/plugins Folder.